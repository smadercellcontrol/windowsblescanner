﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversalBeacon.Library.Core.Entities;
using UniversalBeacon.Library.Core.Interfaces;
using Windows;
using Windows.Devices.Bluetooth.Advertisement;

namespace BLE_UWP_App
{
    class BLeWatch
    {
        //BeaconManager _beaconManager;

        static void Main(string[] args)
        {
            // Start the program
            var program = new BLeWatch();

            // Close on key press
            Console.ReadLine();
        }

        public BLeWatch()
        {
            // Create Bluetooth Listener
            var watcher = new BluetoothLEAdvertisementWatcher();
            watcher.ScanningMode = BluetoothLEScanningMode.Active;

            // Only activate the watcher when we're recieving values >= -50
            watcher.SignalStrengthFilter.InRangeThresholdInDBm = -50;

            // Stop watching if the value drops below -90 (user walked away)
            watcher.SignalStrengthFilter.OutOfRangeThresholdInDBm = -90;
            
            // Register callback for when we see an advertisements
            watcher.Received += OnAdvertisementReceived;

            // Wait 5 seconds to make sure the device is really out of range
            watcher.SignalStrengthFilter.OutOfRangeTimeout = TimeSpan.FromMilliseconds(5000);
            watcher.SignalStrengthFilter.SamplingInterval = TimeSpan.FromMilliseconds(2000);

            //_beaconManager.BeaconAdded += BeaconManagerOnBeaconAdded;
            // Starting watching for advertisements
            watcher.Start();
           
            //endless loop
            while(true)
            {
                //wait
            }
        }

        private void OnAdvertisementReceived(BluetoothLEAdvertisementWatcher watcher, BluetoothLEAdvertisementReceivedEventArgs eventArgs)
        {
            var logPath = System.IO.Path.GetTempPath() + "log.txt";
            using (var logFile = System.IO.File.AppendText(logPath))
            {
                logFile.WriteLine(String.Format("Advertisement:"));
                logFile.WriteLine(String.Format("  BT_ADDR: {0}", eventArgs.BluetoothAddress));
                logFile.WriteLine(String.Format("  FR_NAME: {0}", eventArgs.Advertisement.LocalName));
                logFile.WriteLine();
            }
           
            // Tell the user we see an advertisement and print some properties
            Debug.WriteLine(String.Format("Advertisement:"));
            Debug.WriteLine(String.Format("  BT_ADDR: {0}", eventArgs.BluetoothAddress));
            Debug.WriteLine(String.Format("  FR_NAME: {0}", eventArgs.Advertisement.LocalName));
            Debug.WriteLine("");
        }

        // Call this method e.g., when tapping a button
        private void BeaconManagerOnBeaconAdded(object sender, Beacon beacon)
        {
            Debug.WriteLine("\nBeacon: " + beacon.BluetoothAddressAsString);
            Debug.WriteLine("Type: " + beacon.BeaconType);
            Debug.WriteLine("Last Update: " + beacon.Timestamp);
            Debug.WriteLine("RSSI: " + beacon.Rssi);
            foreach (var beaconFrame in beacon.BeaconFrames.ToList())
            {
                // Print a small sample of the available data parsed by the library
                if (beaconFrame is UidEddystoneFrame)
                {
                    Debug.WriteLine("Eddystone UID Frame");
                    Debug.WriteLine("ID: " + ((UidEddystoneFrame)beaconFrame).NamespaceIdAsNumber.ToString("X") + " / " +
                                    ((UidEddystoneFrame)beaconFrame).InstanceIdAsNumber.ToString("X"));
                }
                else if (beaconFrame is UrlEddystoneFrame)
                {
                    Debug.WriteLine("Eddystone URL Frame");
                    Debug.WriteLine("URL: " + ((UrlEddystoneFrame)beaconFrame).CompleteUrl);
                }
                else if (beaconFrame is TlmEddystoneFrame)
                {
                    Debug.WriteLine("Eddystone Telemetry Frame");
                    Debug.WriteLine("Temperature [°C]: " + ((TlmEddystoneFrame)beaconFrame).TemperatureInC);
                    Debug.WriteLine("Battery [mV]: " + ((TlmEddystoneFrame)beaconFrame).BatteryInMilliV);
                }
                else if (beaconFrame is EidEddystoneFrame)
                {
                    Debug.WriteLine("Eddystone EID Frame");
                    Debug.WriteLine("Ranging Data: " + ((EidEddystoneFrame)beaconFrame).RangingData);
                    Debug.WriteLine("Ephemeral Identifier: " + BitConverter.ToString(((EidEddystoneFrame)beaconFrame).EphemeralIdentifier));
                }
                else if (beaconFrame is ProximityBeaconFrame)
                {
                    Debug.WriteLine("Proximity Beacon Frame (iBeacon compatible)");
                    Debug.WriteLine("Uuid: " + ((ProximityBeaconFrame)beaconFrame).UuidAsString);
                    Debug.WriteLine("Major: " + ((ProximityBeaconFrame)beaconFrame).MajorAsString);
                    Debug.WriteLine("Major: " + ((ProximityBeaconFrame)beaconFrame).MinorAsString);
                }
                else
                {
                    Debug.WriteLine("Unknown frame - not parsed by the library, write your own derived beacon frame type!");
                    Debug.WriteLine("Payload: " + BitConverter.ToString(((UnknownBeaconFrame)beaconFrame).Payload));
                }
            }
        }
    }
}
